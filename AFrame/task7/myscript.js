var zerod = 0;
var images = ["frames/caust_001.png",
            "frames/caust_002.png",
            "frames/caust_003.png",
            "frames/caust_004.png",
            "frames/caust_005.png",
            "frames/caust_006.png",
            "frames/caust_007.png",
            "frames/caust_008.png",
            "frames/caust_009.png",
            "frames/caust_010.png",
            "frames/caust_011.png",
            "frames/caust_012.png",
            "frames/caust_013.png",
            "frames/caust_014.png",
            "frames/caust_015.png",
            "frames/caust_016.png"]
function goNext() {
    zerod = (zerod < images.length - 1) ? ++zerod : 0;
    document.getElementById("glasswall1").setAttribute("src", images[zerod]);
    document.getElementById("glasswall2").setAttribute("src", images[zerod]);
}
const dailyUpdatesInterval = setInterval(goNext, 100);
