﻿namespace task27 {
    class Program {
        static void Main(string[] args) {
            int w, h, n;
            using (TextReader reader = File.OpenText("input1.txt")){
                string[] bits = reader.ReadLine().Split(' ');
                w = int.Parse(bits[0]);
                h = int.Parse(bits[1]);
                bool[,] painting = new bool[w, h];
                for (int i = 0; i < w; i++){
                    for (int j = 0; j < h; j++){
                        painting[i, j] = true;
                    }
                }

                n = int.Parse(reader.ReadLine());
                int x1, y1, x2, y2;
                for (int i = 0; i < n; i++){
                    bits = reader.ReadLine().Split(' ');
                    x1 = int.Parse(bits[0]);
                    y1 = int.Parse(bits[1]);
                    x2 = int.Parse(bits[2])-1;
                    y2 = int.Parse(bits[3])-1;
                    for (int x = x1; x <= x2; x++){
                        for (int y = y1; y <= y2; y++){
                            painting[x, y] = false;
                        }                       
                    }
                }

                int ans = 0;
                for (int i = 0; i < w; i++){
                    for (int j = 0; j < h; j++){
                        if (painting[i, j]) {
                            ans++;
                        }
                    }
                }

                using (StreamWriter writer = new StreamWriter("output.txt")){
                    writer.Write(ans);
                }
            }
        }
    }
}
