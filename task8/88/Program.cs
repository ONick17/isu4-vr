﻿namespace task88 {
    class Program {
        static bool[] ResetCheck(bool[] check){
            for (int i = 0; i < check.GetLength(0); i++){
                check[i] = true;
            }
            return check;
        }
        static void Main(string[] args) {
            int n, m;
            using (TextReader reader = File.OpenText("input1.txt")){
                n = int.Parse(reader.ReadLine());
                m = n*n;
                int[,] sudoku = new int[m, m];
                string[] bits;
                for (int i = 0; i < m; i++){
                    bits = reader.ReadLine().Split(' ');
                    for (int j = 0; j < m; j++){
                        sudoku[i, j] = int.Parse(bits[j]);
                    }
                }

                bool[] check = new bool[m];
                check = ResetCheck(check);
                bool ans = true;
                
                // проверка горизонталей
                for (int i = 0; i < m; i++){
                    for (int j = 0; j < m; j++){
                        if ((sudoku[i, j] <= m) & (sudoku[i, j] > 0)){
                            check[sudoku[i, j]-1] = false;
                        } else {
                            ans = false;
                            break;
                        }
                    }
                    for (int j = 0; j < m; j++){
                        if (check[j]){
                            ans = false;
                            break;
                        }
                    }
                    check = ResetCheck(check);
                }

                // проверка вертикалей
                if (ans) {
                    for (int i = 0; i < m; i++){
                        for (int j = 0; j < m; j++){
                            check[sudoku[j, i]-1] = false;
                        }
                        for (int j = 0; j < m; j++){
                            if (check[j]){
                                ans = false;
                                break;
                            }
                        }
                        check = ResetCheck(check);
                    }
                }

                // проверка квадратов
                if (ans) {
                    for (int a = 0; a < m; a++){
                        for (int i = 0; i < n; i++){
                            for (int j = 0; j < n; j++){
                                if (sudoku[i, j] <= m){
                                    check[sudoku[j, i]-1] = false;
                                } else {
                                    ans = false;
                                    break;
                                }
                            }
                        }
                        for (int j = 0; j < m; j++){
                            if (check[j]){
                                ans = false;
                                break;
                            }
                        }
                        check = ResetCheck(check);
                    }
                }

                using (StreamWriter writer = new StreamWriter("output.txt")){
                    if (ans) {
                        writer.Write("Correct");
                    } else {
                        writer.Write("Incorrect");
                    }
                }
            }
        }
    }
}
