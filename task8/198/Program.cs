﻿using System.Reflection;

namespace task198 {
    class Program {
        static int FindDelta(int n, int[,] nmbrs, int mode){
            if (n == 2) {
                if (mode == 0) {
                    return nmbrs[0, 2]*nmbrs[1, 1]-nmbrs[0, 1]*nmbrs[1, 2];
                } else if (mode == 1) {
                    return nmbrs[0, 0]*nmbrs[1, 2]-nmbrs[0, 2]*nmbrs[1, 0];
                } else {
                    return nmbrs[0, 0]*nmbrs[1, 1]-nmbrs[0, 1]*nmbrs[1, 0];
                }
            }
            int delta = 0;
            int keke;
            int j2;
            for (int j = 0; j < n; j++){
                keke = 1;
                for (int i = 0; i < n; i++){
                    j2 = (j+i)%n;
                    if (j2 == mode) {
                        j2 = n;
                    }
                    keke *= nmbrs[i, j2];
                }
                delta += keke;

                keke = 1;
                for (int i = 0; i < n; i++){
                    if (j-i < 0) {
                        j2 = n-(i-j);
                    } else {
                        j2 = j-i;
                    }
                    if (j2 == mode) {
                        j2 = n;
                    }
                    keke *= nmbrs[i, j2];
                }
                delta -= keke;
            }
            return delta;
        }

        static void Main(string[] args) {
            int n;
            using (TextReader reader = File.OpenText("input1.txt")){
                n = int.Parse(reader.ReadLine());
                int[,] nmbrs = new int[n, n+1];
                int[] anss = new int[n];
                string[] bits;
                for (int i = 0; i < n; i++){
                    bits = reader.ReadLine().Split();
                    for (int j = 0; j < n+1; j++){
                        nmbrs[i, j] = int.Parse(bits[j]);
                    }
                }

                int delta = FindDelta(n, nmbrs, -1);

                for (int i = 0; i < n; i++){
                    anss[i] = FindDelta(n, nmbrs, i);
                    anss[i] = anss[i]/delta;
                }

                using (StreamWriter writer = new StreamWriter("output.txt")){
                    for (int i = 0; i < n; i++){
                        writer.Write(anss[i]);
                        writer.Write(" ");
                    }
                }
            }
        }
    }
}
