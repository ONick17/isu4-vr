using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Controls target objects behaviour.
/// </summary>
public class Switch : MonoBehaviour
{
    /// <summary>
    /// The material to use when this object is inactive (not being gazed at).
    /// </summary>
    public Material InactiveMaterial;

    /// <summary>
    /// The material to use when this object is active (gazed at).
    /// </summary>
    public Material GazedAtMaterial;
    
    private Renderer _myRenderer;

    GameObject[] emmisions;
    GameObject lamp;
    GameObject[] wall_lamps;
    bool a;


    /// <summary>
    /// Start is called before the first frame update.
    /// </summary>
    public void Start()
    {
        _myRenderer = GetComponent<Renderer>();
        SetMaterial(false);
        lamp = GameObject.FindWithTag("lamp");
        wall_lamps = GameObject.FindGameObjectsWithTag("wall_lamp");
        emmisions = GameObject.FindGameObjectsWithTag("emission");
        a = true;
    }

    /// <summary>
    /// This method is called by the Main Camera when it starts gazing at this GameObject.
    /// </summary>
    public void OnPointerEnter()
    {
        SetMaterial(true);
    }

    /// <summary>
    /// This method is called by the Main Camera when it stops gazing at this GameObject.
    /// </summary>
    public void OnPointerExit()
    {
        SetMaterial(false);
    }

    /// <summary>
    /// This method is called by the Main Camera when it is gazing at this GameObject and the screen
    /// is touched.
    /// </summary>
    public void OnPointerClick()
    {
        a = !a;
        if (a) {
            transform.position = new Vector3(-1.15f, 3.455f, -2.985f);
            transform.rotation = Quaternion.Euler(9.5f, 0, 0);
        } else {
            transform.position = new Vector3(-1.532f, 3.3765f, -2.985f);
            transform.rotation = Quaternion.Euler(-9.5f, 0, 180);
        }
        switchLight(a);
    }

    private void switchLight(bool a)
    {
        if (a) {
            foreach (GameObject emmision in emmisions)
            {
                emmision.GetComponent<Renderer>().materials[0].EnableKeyword("_EMISSION");
            }
        } else {
            foreach (GameObject emmision in emmisions)
            {
                emmision.GetComponent<Renderer>().materials[0].DisableKeyword("_EMISSION");
            }
        }
        foreach (GameObject wall_lamp in wall_lamps)
        {
            wall_lamp.GetComponent<Light>().enabled = a;
        }
        lamp.GetComponent<Light>().enabled = a;

    }

    /// <summary>
    /// Sets this instance's material according to gazedAt status.
    /// </summary>
    /// <param name="gazedAt">
    /// Value `true` if this object is being gazed at, `false` otherwise.
    /// </param>
    private void SetMaterial(bool gazedAt)
    {
        if (InactiveMaterial != null && GazedAtMaterial != null)
        {
            _myRenderer.material = gazedAt ? GazedAtMaterial : InactiveMaterial;
        }
    }
}
