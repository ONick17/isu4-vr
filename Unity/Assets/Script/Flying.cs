using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Flying : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ChangePosition", 0, 0.025f);
    }

    int a = 1;
    public double up;
    public double down;

    void ChangePosition() {
        transform.position = new Vector3 (transform.position.x, transform.position.y+0.01f*a, transform.position.z);
        if (transform.position.y > up) {
            a = -1;
        } else if (transform.position.y < down) {
            a = 1;
        }
    }
}
