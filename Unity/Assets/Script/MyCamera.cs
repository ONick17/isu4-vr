using System;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Draws a circular reticle in front of any object that the user points at.
/// </summary>
/// <remarks>
/// Sends messages to gazed GameObject. The reticle dilates if the object has an interactive layer.
/// </remarks>
public class MyCamera : MonoBehaviour
{
    /// <summary>
    /// Sorting order to use for the reticle's renderer.
    /// </summary>
    /// <remarks><para>
    /// Range values come from https://docs.unity3d.com/ScriptReference/Renderer-sortingOrder.html.
    /// </para><para>
    /// Default value 32767 ensures gaze reticle is always rendered on top.
    /// </para></remarks>
    [Range(-32767, 32767)]
    public int ReticleSortingOrder = 32767;

    /// <summary>
    /// Mask used to indicate interactive objects.
    /// </summary>
    public LayerMask ReticleInteractionLayerMask = 1 << _RETICLE_INTERACTION_DEFAULT_LAYER;

    /// <summary>
    /// Default layer for interactive game objects.
    /// </summary>
    private const int _RETICLE_INTERACTION_DEFAULT_LAYER = 8;

    /// <summary>
    /// Number of segments making the reticle circle.
    /// </summary>
    private const int _RETICLE_SEGMENTS = 20;

    /// <summary>
    /// The game object the reticle is pointing at.
    /// </summary>
    private GameObject _gazedAtObject = null;

    GameObject player;

    /// <summary>
    /// Start is called before the first frame update.
    /// </summary>
    private void Start()
    {
        // Renderer rendererComponent = GetComponent<Renderer>();
        // rendererComponent.sortingOrder = ReticleSortingOrder;

        player = GameObject.FindWithTag("Player");
        // CreateMesh();
        CreateMesh();
    }

    public LayerMask mask;

    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    private void Update()
    {
        // Casts ray towards camera's forward direction, to detect if a GameObject is being gazed at.
        var ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            GameObject hit_obj = hit.collider.gameObject;
            // Debug.Log($"looking at {hit_obj.name}", this);
            // var hit_obj = hit.transform.gameObject;
            // GameObject detected in front of the camera.
            if (_gazedAtObject != hit_obj)
            {
                // New GameObject.
                if(_gazedAtObject?.GetComponent<Teleporter>() != null)
                {
                    // _gazedAtObject?.SendMessage("OnPointerExit");
                    _gazedAtObject?.GetComponent<Teleporter>().OnPointerExit();
                }
                if(_gazedAtObject?.GetComponent<MusicChange>() != null)
                {
                    // _gazedAtObject?.SendMessage("OnPointerExit");
                    _gazedAtObject?.GetComponent<MusicChange>().OnPointerExit();
                }
                if(_gazedAtObject?.GetComponent<Switch>() != null)
                {
                    // _gazedAtObject?.SendMessage("OnPointerExit");
                    _gazedAtObject?.GetComponent<Switch>().OnPointerExit();
                }

                _gazedAtObject = hit_obj;
                if(_gazedAtObject?.GetComponent<Teleporter>() != null)
                {
                    // _gazedAtObject?.SendMessage("OnPointerEnter");
                    _gazedAtObject?.GetComponent<Teleporter>().OnPointerEnter();
                }
                if(_gazedAtObject?.GetComponent<MusicChange>() != null)
                {
                    // _gazedAtObject?.SendMessage("OnPointerEnter");
                    _gazedAtObject?.GetComponent<MusicChange>().OnPointerEnter();
                }
                if(_gazedAtObject?.GetComponent<Switch>() != null)
                {
                    // _gazedAtObject?.SendMessage("OnPointerEnter");
                    _gazedAtObject?.GetComponent<Switch>().OnPointerEnter();
                }
            }
        }
        else
        {
            // No GameObject detected in front of the camera.
            if(_gazedAtObject?.GetComponent<Teleporter>() != null)
            {
                // _gazedAtObject?.SendMessage("OnPointerExit");
                _gazedAtObject?.GetComponent<Teleporter>().OnPointerExit();
            }
            if(_gazedAtObject?.GetComponent<MusicChange>() != null)
            {
                // _gazedAtObject?.SendMessage("OnPointerExit");
                _gazedAtObject?.GetComponent<MusicChange>().OnPointerExit();
            }
            if(_gazedAtObject?.GetComponent<Switch>() != null)
            {
                // _gazedAtObject?.SendMessage("OnPointerExit");
                _gazedAtObject?.GetComponent<Switch>().OnPointerExit();
            }
            _gazedAtObject = null;
        }
        // Checks for screen touches.
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            if(_gazedAtObject?.GetComponent<Teleporter>() != null)
            {
                // _gazedAtObject?.SendMessage("OnPointerClick");
                _gazedAtObject?.GetComponent<Teleporter>().OnPointerClick();
            }
            if(_gazedAtObject?.GetComponent<MusicChange>() != null)
            {
                // _gazedAtObject?.SendMessage("OnPointerClick");
                _gazedAtObject?.GetComponent<MusicChange>().OnPointerClick();
            }
            if(_gazedAtObject?.GetComponent<Switch>() != null)
            {
                // _gazedAtObject?.SendMessage("OnPointerClick");
                _gazedAtObject?.GetComponent<Switch>().OnPointerClick();
            }
        }
    }

    private void CreateMesh()
    {
        Mesh mesh = new Mesh();
        gameObject.AddComponent<MeshFilter>();
        GetComponent<MeshFilter>().mesh = mesh;

        int segments_count = _RETICLE_SEGMENTS;
        int vertex_count = (segments_count + 1) * 2;

        // Vertices.
        Vector3[] vertices = new Vector3[vertex_count];

        const float kTwoPi = Mathf.PI * 2.0f;
        int vi = 0;
        for (int si = 0; si <= segments_count; ++si)
        {
            // Add two vertices for every circle segment: one at the beginning of the
            // prism, and one at the end of the prism.
            float angle = (float)si / (float)segments_count * kTwoPi;

            float x = Mathf.Sin(angle);
            float y = Mathf.Cos(angle);

            vertices[vi++] = new Vector3(x, y, 0.0f); // Outer vertex.
            vertices[vi++] = new Vector3(x, y, 1.0f); // Inner vertex.
        }

        // Triangles.
        int indices_count = (segments_count + 1) * 3 * 2;
        int[] indices = new int[indices_count];

        int vert = 0;
        int idx = 0;
        for (int si = 0; si < segments_count; ++si)
        {
            indices[idx++] = vert + 1;
            indices[idx++] = vert;
            indices[idx++] = vert + 2;

            indices[idx++] = vert + 1;
            indices[idx++] = vert + 2;
            indices[idx++] = vert + 3;

            vert += 2;
        }

        mesh.vertices = vertices;
        mesh.triangles = indices;
        mesh.RecalculateBounds();
    }

    /// <summary>
    /// Evaluates if the provided GameObject is interactive based on its layer.
    /// </summary>
    ///
    /// <param name="gameObject">The game object on which to check if its layer is interactive.</param>
    ///
    /// <returns>Whether or not a GameObject's layer is interactive.</returns>
    private bool IsInteractive(GameObject gameObject)
    {
        // return (1 << gameObject?.layer & ReticleInteractionLayerMask) != 0;
        if(gameObject?.GetComponent<Teleporter>() != null)
        {
            return true;
        }
        return false;
    }
}